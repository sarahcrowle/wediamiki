from setuptools import setup

setup(
   name='wediamiki',
   version='0.1',
   description='A thin wrapper around the mediawiki action api',
   author='Sarah Crowle',
   author_email='',
   packages=['wediamiki'],  #same as name
   install_requires=['requests', 'python-magic'], #external packages as dependencies
)
