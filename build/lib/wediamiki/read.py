from wiki_connection import WikiConnection
import datetime

def change_authentication_data(wiki: WikiConnection, 
                               change_auth_request: str, 
                               change_auth_token: str, 
                               params: dict[str, str]):
    return wiki.api("changeauthenticationdata", {
        "changeauthrequest": change_auth_request,
        "changeauthtoken": change_auth_token,
        "params": params
    })

def check_token(wiki: WikiConnection, 
                token_type: str,  # name changed
                token: str, 
                max_token_age: int):
    return wiki.api("checktoken", {
        "type": token_type,
        "token": token,
        "maxtokenage": max_token_age
    })

# I am suffering
def compare(wiki: WikiConnection, 
            from_title: str = None, 
            from_id: int = None, 
            from_rev: int = None, 
            from_slots: list[str] = None, 
            from_text: dict[str, str] = None, 
            from_section: dict[str, str] = None, 
            from_content_format: dict[str, str] = None, 
            from_content_model: dict[str, str] = None, 
            from_pst: bool = False, 
            to_title: str = None, 
            to_id: int = None, 
            to_rev: int = None, 
            to_relative: str = None, 
            to_slots: list[str] = None, 
            to_text: dict[str, str] = None, 
            to_section: dict[str, str] = None, 
            to_content_format: dict[str, str] = None, 
            to_content_model: dict[str, str] = None, 
            to_pst: bool = False, 
            prop: list[str] = ["diff", "ids", "title"], 
            slots: list[str] = None):
    return wiki.api("compare", {
        "fromtitle": from_title,
        "fromid": from_id,
        "fromrev": from_rev,
        "fromslots": from_slots,
        "fromtext": from_text,
        "fromsection": from_section,
        "fromcontentformat": from_content_format,
        "fromcontentmodel": from_content_model,
        "frompst": from_pst,
        "totitle": to_title,
        "toid": to_id,
        "torev": to_rev,
        "torelative": to_relative,
        "toslots": to_slots,
        "totext": to_text,
        "tosection": to_section,
        "tocontentformat": to_content_format,
        "tocontentmodel": to_content_model,
        "topst": to_pst,
        "prop": prop,
        "slots": slots
    })

def expand_templates(wiki: WikiConnection,
                     title: str,
                     text: str,
                     revision_id: int = None,
                     prop: list[str] = None,
                     include_comments: bool = None,
                     show_strategy_keys: bool = None,
                     template_sandbox_prefix: list[str] = None,
                     template_sandbox_title: str = None,
                     template_sandbox_text: str = None,
                     template_sandbox_content_model: str = None,
                     template_sandbox_content_format: str = None):
    return wiki.api("expandtemplates", {
        "title": title,
        "text": text,
        "revid": revision_id,
        "prop": prop,
        "includecomments": include_comments,
        "showstrategykeys": show_strategy_keys,
        "templatesandboxprefix": template_sandbox_prefix,
        "templatesandboxtitle": template_sandbox_title,
        "templatesandboxtext": template_sandbox_text,
        "templatesandboxcontentmodel": template_sandbox_content_model,
        "templatesandboxcontentformat": template_sandbox_content_format
    })

def feed_contributions(wiki: WikiConnection,
                       user: str,
                       feed_format: str = "rss",
                       namespace: int = None,
                       year: int = None,
                       month: int = None,
                       tag_filter: list[str] = [],
                       deleted_only: bool = False,
                       top_only: bool = False,
                       new_only: bool = False,
                       hide_minor: bool = False,
                       show_size_diff: bool = False):
    return wiki.api("feedcontributions", {
        "user": user,
        "feedformat": feed_format,
        "namespace": namespace,
        "year": year,
        "month": month,
        "tagfilter": tag_filter,
        "deletedonly": deleted_only,
        "toponly": top_only,
        "newonly": new_only,
        "hideminor": hide_minor,
        "showsizediff": show_size_diff
    })

def feed_recent_changes(wiki: WikiConnection,
                        feed_format: str = "rss",
                        namespace: int = None,
                        invert: bool = False,
                        associated: bool = False,
                        days: int = 7,
                        limit: int = 50,
                        show_from: datetime.datetime = None, # NOTE: name changed due to reserved word
                        hide_minor: bool = False,
                        hide_bots: bool = False,
                        hide_anons: bool = False,
                        hide_liu: bool = False,
                        hide_patrolled: bool = False,
                        hide_myself: bool = False,
                        hide_categorization: bool = False,
                        tag_filter: list[str] = [],
                        invert_tags: bool = False,
                        target: str = None,
                        show_linked_to: bool = False):
    return wiki.api("feedrecentchanges", {
        "feedformat": feed_format,
        "namespace": namespace,
        "invert": invert,
        "associated": associated,
        "days": days,
        "limit": limit,
        "from": show_from,
        "hideminor": hide_minor,
        "hidebots": hide_bots,
        "hideliu": hide_liu,
        "hideanons": hide_anons,
        "hidepatrolled": hide_patrolled,
        "hidemyself": hide_myself,
        "hidecategorization": hide_categorization,
        "tagfilter": tag_filter,
        "inverttags": invert_tags,
        "target": target,
        "showlinkedto": show_linked_to
    })

def feed_watchlist(wiki: WikiConnection,
                   feed_format: str = "rss",
                   hours: int = 24,
                   link_to_sections: bool = False,
                   all_rev: bool = False,
                   wl_owner: str = None,
                   wl_token: str = None,
                   wl_show: list[str] = None,
                   wl_type: list[str] = ["edit", "new", "log", "categorize"],
                   wl_exclude_user: str = None):
    return wiki.api("feedwatchlist", {
        "feedformat": feed_format,
        "hours": hours,
        "linktosections": link_to_sections,
        "allrev": all_rev,
        "wlowner": wl_owner,
        "wltoken": wl_token,
        "wlshow": wl_show,
        "wltype": wl_type,
        "wlexcludeuser": wl_exclude_user
    })

def opensearch(wiki: WikiConnection,
               search: str,
               namespace: list[int] = 0,
               limit: int = 10,
               profile: str = "engine_autoselect",
               redirects: str = "return",
               return_format: str = "json",
               warnings_as_error: bool = False):
    return wiki.api("opensearch", {
        "search": search,
        "namespace": namespace,
        "limit": limit,
        "profile": profile,
        "redirects": redirects,
        "format": return_format,
        "warningsaserror": warnings_as_error
    })

def param_info(wiki: WikiConnection,
               modules: list[str],
               help_format: str = "none"):
    return wiki.api("paraminfo", {
        "modules": modules,
        "helpformat": help_format
    })

def parse(wiki: WikiConnection,
          title: str = None,
          rev_id: int = None,
          summary: str = None,
          page: str = None,
          page_id: int = None,
          redirects: bool = False,
          old_id: int = None,
          prop: list[str] = ["text", "langlinks", "categories", "links", "templates", "images", "externallinks", "sections", "revid", "displaytitle", "iwlinks", "properties", "parsewarnings"],
          wrap_output_class: str = "mw-parser-output",
          pst: bool = False,
          only_pst: bool = False,
          section: str = None,
          section_title: str = None,
          disable_limit_report: bool = False,
          disable_edit_section: bool = False,
          disable_style_deduplication: bool = False,
          show_strategy_keys: bool = False,
          preview: bool = False,
          section_preview: bool = False,
          disable_toc: bool = False,
          use_skin: str = None,
          content_format: str = None,
          content_model: str = None,
          mobile_format: bool = False,
          template_sandbox_prefix: list[str] = None,
          template_sandbox_title: str = None,
          template_sandbox_text: str = None,
          template_sandbox_content_model: str = None,
          template_sandbox_content_format: str = None):
    return wiki.api("parse", {
        "title": title,
        "revid": rev_id,
        "summary": summary,
        "page": page,
        "page_id": page_id,
        "redirects": redirects,
        "oldid": old_id,
        "prop": prop,
        "wrapoutputclass": wrap_output_class,
        "pst": pst,
        "onlypst": only_pst,
        "section": section,
        "sectiontitle": section_title,
        "disablelimitreport": disable_limit_report,
        "disableeditsection": disable_edit_section,
        "disablestylededuplication": disable_style_deduplication,
        "showstrategykeys": show_strategy_keys,
        "preview": preview,
        "sectionpreview": section_preview,
        "disabletoc": disable_toc,
        "useskin": use_skin,
        "contentformat": content_format,
        "contentmodel": content_model,
        "mobileformat": mobile_format,
        "templatesandboxprefix": template_sandbox_prefix,
        "templatesandboxtitle": template_sandbox_title,
        "templatesandboxtext": template_sandbox_text,
        "templatesandboxcontentmodel": template_sandbox_content_model,
        "templatesandboxcontentformat": template_sandbox_content_format
    })

def query(wiki: WikiConnection,
          prop: list[str] = None,
          q_list: list[str] = None,
          meta: list[str] = None,
          index_page_ids: bool = None,
          export: bool = None,
          export_no_wrap: bool = None,
          export_schema: str = "0.10",
          iw_url: bool = None,
          should_continue: bool = False,
          raw_continue: bool = False,
          titles: list[str] = None,
          page_ids: list[int] = None,
          rev_ids: list[int] = None,
          generator: str = None,
          redirects: bool = False,
          convert_titles: bool = False):
    return wiki.api("query", {
        "prop": prop,
        "list": q_list,
        "meta": meta,
        "indexpageids": index_page_ids,
        "export": export,
        "exportnowrap": export_no_wrap,
        "exportschema": export_schema,
        "iwurl": iw_url,
        "continue": should_continue,
        "rawcontinue": raw_continue,
        "titles": titles,
        "pageids": page_ids,
        "revids": rev_ids,
        "generator": generator,
        "redirects": redirects,
        "converttitles": convert_titles
    })

def remove_authentication_data(wiki: WikiConnection,
                               change_auth_request: str,
                               change_auth_token: str,
                               params: dict[str, str] = None):
    return wiki.api("removeauthenticationdata", {
        "changeauthrequest": change_auth_request,
        "changeauthtoken": change_auth_token,
        "params": params
    })

def rsd(wiki: WikiConnection):
    return wiki.api("rsd", {})

def stash_edit(wiki: WikiConnection,
               content_model: str,
               content_format: str,
               base_rev_id: int,
               token: str,
               title: str,
               section: str = None,
               section_title: str = None,
               text: str = None,
               stashed_text_hash: str = None,
               summary: str = ""):
    return wiki.api("stashedit", {
        "contentmodel": content_model,
        "contentformat": content_format,
        "baserevid": base_rev_id,
        "token": token,
        "title": title,
        "section": section,
        "sectiontitle": section_title,
        "text": text,
        "stashedtexthash": stashed_text_hash,
        "summary": summary
    })

def unlink_account(wiki: WikiConnection,
                   change_auth_request: str,
                   change_auth_token: str,
                   params: dict[str, str] = None):
    return wiki.api("unlinkaccount", {
        "changeauthrequest": change_auth_request,
        "changeauthtoken": change_auth_token,
        "params": params
    })