import requests
import datetime
import time
import magic

class WikiConnection:
    """A connection to a MediaWiki wiki"""
    def __init__(self, wiki_url, wiki_username, wiki_password, csrf=False, test_endpoint=None):
        self.wiki_url = wiki_url
        self.wiki_username = wiki_username
        self.wiki_password = wiki_password
        self.session = requests.Session()
        self.test_endpoint = test_endpoint
        if self.test_endpoint:
            self.test_mode = True
        else:
            self.test_mode = False
        
        self.connect(csrf)
    
    def connect(self, csrf):
        # get our login token
        login_token_params = {
            "action": "query",
            "meta": "tokens",
            "type": "login",
            "format": "json"
        }

        response = self.session.get(url=self.wiki_url, params=login_token_params)
        self.login_token = response.json()["query"]["tokens"]["logintoken"]

        # now log in
        login_params = {
            "action": "login",
            "lgname": self.wiki_username,
            "lgpassword": self.wiki_password,
            "lgtoken": self.login_token,
            "format": "json"
        }

        response = self.session.post(self.wiki_url, data=login_params)
        assert response.json()["login"]["result"] == "Success"

        # let's get a csrf token, too, if the user wants one
        if csrf:
            csrf_token_params = {
                "action": "query",
                "meta": "tokens",
                "format": "json"
            }
            response = self.session.get(url=self.wiki_url, params=csrf_token_params)
            # FIXME: no way to determine success here?
            self.csrf_token = response.json()["query"]["tokens"]["csrftoken"]

        if self.test_mode:
            self.wiki_url = self.test_endpoint
    
    def api(self, action, params):
        sanitary_params = {}
        has_file = False
        file_io = None
        file_source = ""
        for k, v in params.items():
            if isinstance(v, bool):
                if v:
                    sanitary_params[k] = ""
            
            if k == "file":
                has_file = True
                file_io = open(v, "rb")
                file_source = v

            if v:
                if isinstance(v, list):
                    sanitary_params[k] = "|".join(v)
                
                if isinstance(v, dict):
                    if k == "params":
                        sanitary_params.update(v)
                    else: # for stuff like the slots in compare calls
                        for kk, vv in v.items():
                            sanitary_params[k + "-" + kk] = vv
                
                if isinstance(v, str):
                    sanitary_params[k] = v
                
                if isinstance(v, int):
                    sanitary_params[k] = str(v)
                
                if isinstance(v, datetime.datetime):
                    sanitary_params[k] = str((time.mktime(v.timetuple())))
        
        sanitary_params["format"] = "json"
        sanitary_params["action"] = action
        if not has_file:
            response = self.session.post(self.wiki_url, data=sanitary_params)
        else:
            mime = magic.Magic(mime=True)
            mimetype = mime.from_file(file_source)
            files = {
                "file": (sanitary_params["filename"], file_io, mimetype, {'Expires': '0'})
            }
            headers = {
                "Content-Disposition": f'form-data; name="file"; filename="{sanitary_params["filename"]}"'
            }
            print(files)
            response = self.session.post(self.wiki_url, data=sanitary_params, headers=headers, files=files)
        return response.json()
    
    def api_with_csrf(self, action, params):
        params["token"] = self.csrf_token
        return self.api(action, params)