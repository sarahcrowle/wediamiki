from wiki_connection import WikiConnection
import datetime

def block(wiki: WikiConnection,
          user: str,
          expiry: str = "never",
          reason: str = "",
          anon_only: bool = False,
          no_create: bool = False,
          auto_block: bool = False,
          no_email: bool = False,
          hide_name: bool = False,
          allow_user_talk: bool = False,
          reblock: bool = False,
          watch_user: bool = False,
          watchlist_expiry: datetime.datetime = None,
          tags: list[str] = None,
          partial: bool = False,
          page_restrictions: list[str] = None,
          namespace_restrictions: list[str] = None):
    return wiki.api_with_csrf("block", {
        "user": user,
        "expiry": expiry,
        "reason": reason,
        "anononly": anon_only,
        "nocreate": no_create,
        "autoblock": auto_block,
        "noemail": no_email,
        "hidename": hide_name,
        "allowusertalk": allow_user_talk,
        "reblock": reblock,
        "watchuser": watch_user,
        "watchlistexpiry": watchlist_expiry,
        "tags": tags,
        "partial": partial,
        "pagerestrictions": page_restrictions,
        "namespacerestrictions": namespace_restrictions,
    })

def clear_hasmsg(wiki: WikiConnection):
    return wiki.api("clearhasmsg", {})

def create_account(wiki: WikiConnection,
                   create_token: str,
                   create_requests: list[str] = None,
                   create_message_format: str = "wikitext",
                   create_merge_request_fields: bool = False,
                   create_preserve_state: bool = False,
                   create_return_url: bool = False,
                   create_continue: bool = False,
                   params: dict[str, str] = None):
    return wiki.api("createaccount", {
        "createtoken": create_token,
        "createrequests": create_requests,
        "createmessageformat": create_message_format,
        "createmessagerequestfields": create_merge_request_fields,
        "createpreservestate": create_preserve_state,
        "createreturnurl": create_return_url,
        "createcontinue": create_continue,
        "params": params
    })

def delete(wiki: WikiConnection,
           title: str = None,
           page_id: int = None,
           reason: str = None,
           tags: list[str] = None,
           delete_talk: bool = False,
           watchlist: str = "preferences",
           watchlist_expiry: datetime.datetime = None,
           old_image: str = None):
    return wiki.api_with_csrf("delete", {
        "title": title,
        "pageid": page_id,
        "reason": reason,
        "tags": tags,
        "deletetalk": delete_talk,
        "watchlist": watchlist,
        "watchlistexpiry": watchlist_expiry,
        "oldimage": old_image,
    })

def edit(wiki: WikiConnection,
         title: str = None,
         page_id: int = None,
         section: str = None,
         section_title: str = None,
         text: str = None,
         summary: str = None,
         tags: list[str] = None,
         minor: bool = False,
         not_minor: bool = False,
         bot: bool = False,
         base_rev_id: int = None,
         base_timestamp: datetime.datetime = None,
         start_timestamp: datetime.datetime = None,
         recreate: bool = False,
         create_only: bool = False,
         no_create: bool = False,
         watchlist: str = "preferences",
         watchlist_expiry: datetime.datetime = None,
         md5: str = None,
         prepend_text: str = None,
         append_text: str = None,
         undo: int = None,
         undo_after: int = None,
         redirect: bool = False,
         content_format: str = None,
         content_model: str = None,
         captcha_word: str = None,
         captcha_id: int = None):
    return wiki.api_with_csrf("edit", {
        "title": title,
        "pageid": page_id,
        "section": section,
        "sectiontitle": section_title,
        "text": text,
        "summary": summary,
        "tags": tags,
        "minor": minor,
        "notminor": not_minor,
        "bot": bot,
        "baserevid": base_rev_id,
        "basetimestamp": base_timestamp,
        "starttimestamp": start_timestamp,
        "recreate": recreate,
        "createonly": create_only,
        "nocreate": no_create,
        "watchlist": watchlist,
        "watchlist_expiry": watchlist_expiry,
        "md5": md5,
        "prependtext": prepend_text,
        "appendtext": append_text,
        "undo": undo,
        "undoafter": undo_after,
        "redirect": redirect,
        "contentformat": content_format,
        "contentmodel": content_model,
        "captchaword": captcha_word,
        "captchaid": captcha_id,
    })

def email_user(wiki: WikiConnection,
               target: str,
               subject: str,
               text: str,
               ccme: bool = False):
    return wiki.api_with_csrf("emailuser", {
        "target": target,
        "subject": subject,
        "text": text,
        "ccme": ccme,
    })

def file_revert(wiki: WikiConnection,
                filename: str,
                archive_name: str,
                comment: str = ""):
    return wiki.api_with_csrf("filerevert", {
        "filename": filename,
        "archivename": archive_name,
        "comment": comment,
    })

def image_rotate(wiki: WikiConnection,
                 rotation: int,
                 should_continue: bool = False,
                 tags: list[str] = None,
                 titles: list[str] = None,
                 page_ids: list[int] = None,
                 rev_ids: list[int] = None,
                 generator: str = None,
                 redirects: bool = False,
                 convert_titles: bool = False):
    return wiki.api_with_csrf("imagerotate", {
        "rotation": rotation,
        "continue": should_continue,
        "tags": tags,
        "titles": titles,
        "pageids": page_ids,
        "revids": rev_ids,
        "generator": generator,
        "redirects": redirects,
        "converttitles": convert_titles,
    })

# name changed because python
def import_page(wiki: WikiConnection,
                summary: str = None,
                xml: str = None,
                interwiki_prefix: str = None,
                interwiki_source: str = None,
                interwiki_page: str = None,
                full_history: bool = False,
                templates: bool = False,
                namespace: int = None,
                assign_known_users: bool = False,
                root_page: str = None,
                tags: list[str] = None):
    return wiki.api_with_csrf("import", {
        "summary": summary,
        "xml": xml,
        "interwikiprefix": interwiki_prefix,
        "interwikisource": interwiki_source,
        "interwikipage": interwiki_page,
        "fullhistory": full_history,
        "templates": templates,
        "namespace": namespace,
        "assignknownusers": assign_known_users,
        "rootpage": root_page,
        "tags": tags,
    })

def manage_tags(wiki: WikiConnection,
                operation: str,
                tag: str,
                reason: str = "",
                ignore_warnings: bool = False,
                tags: list[str] = None):
    return wiki.api_with_csrf("managetags", {
        "operation": operation,
        "tag": tag,
        "reason": reason,
        "ignorewarnings": ignore_warnings,
        "tags": tags,
    })

def merge_history(wiki: WikiConnection,
                  from_page: str = None,
                  from_id: int = None,
                  to_page: str = None,
                  to_id: int = None,
                  timestamp: datetime.datetime = None,
                  reason: str = ""):
    return wiki.api_with_csrf("mergehistory", {
        "from": from_page,
        "fromid": from_id,
        "to": to_page,
        "toid": to_id,
        "timestamp": timestamp,
        "reason": reason,
    })

def move(wiki: WikiConnection,
         from_page: str = None,
         from_id: int = None,
         to_page: str = None,
         reason: str = "",
         move_talk: bool = False,
         move_sub_pages: bool = False,
         no_redirect: bool = False,
         watchlist: str = "preferences",
         watchlist_expiry: datetime.datetime = None,
         ignore_warnings: bool = False,
         tags: list[str] = None):
    return wiki.api_with_csrf("move", {
        "from": from_page,
        "fromid": from_id,
        "to": to_page,
        "reason": reason,
        "movetalk": move_talk,
        "movesubpages": move_sub_pages,
        "noredirect": no_redirect,
        "watchlist": watchlist,
        "watchlistexpiry": watchlist_expiry,
        "ignorewarnings": ignore_warnings,
        "tags": tags,
    })

def options(wiki: WikiConnection,
            reset: bool = False,
            reset_kinds: list[str] = ["all"],
            change: list[str] = None,
            option_name: str = None,
            option_value: str = None):
    return wiki.api_with_csrf("options", {
        "reset": reset,
        "resetkinds": reset_kinds,
        "change": change,
        "optionname": option_name,
        "optionvalue": option_value,
    })

def patrol(wiki: WikiConnection,
           rc_id: int,
           rev_id: int,
           tags: int = None):
    return wiki.api_with_csrf("patrol", {
        "rcid": rc_id,
        "revid": rev_id,
        "tags": tags,
    })

def protect(wiki: WikiConnection,
            protections: list[str],
            title: str = None,
            page_id: str = None,
            expiry: list[datetime.datetime] = None,
            reason: str = "",
            tags: list[str] = None,
            cascade: bool = False,
            watchlist: str = "preferences",
            watchlist_expiry: datetime.datetime = None):
    return wiki.api_with_csrf("protect", {
        "protections": protections,
        "title": title,
        "pageid": page_id,
        "expiry": expiry,
        "reason": reason,
        "tags": tags,
        "cascade": cascade,
        "watchlist": watchlist,
        "watchlistexpiry": watchlist_expiry,
    })

def purge(wiki: WikiConnection,
          force_link_update: bool = False,
          force_recursive_link_update: bool = False,
          should_continue: bool = False,
          titles: list[str] = None,
          page_ids: list[int] = None,
          rev_ids: list[int] = None,
          generator: str = None,
          redirects: bool = False,
          convert_titles: bool = False):
    return wiki.api("purge", {
        "forcelinkupdate": force_link_update,
        "forcerecursivelinkupdate": force_recursive_link_update,
        "continue": should_continue,
        "titles": titles,
        "pageids": page_ids,
        "revids": rev_ids,
        "generator": generator,
        "redirects": redirects,
        "converttitles": convert_titles
    })

def reset_password(wiki: WikiConnection,
                   user: str = None,
                   email: str = None):
    return wiki.api_with_csrf("resetpassword", {
        "user": user,
        "email": email,
    })

def revision_delete(wiki: WikiConnection,
                    rev_type: str,
                    target: str = None,
                    ids: list[int] = None,
                    hide: list[str] = None,
                    show: list[str] = None,
                    suppress: str = "nochange",
                    reason: str = None,
                    tags: list[str] = None):
    return wiki.api_with_csrf("revisiondelete", {
        "type": rev_type,
        "target": target,
        "ids": ids,
        "hide": hide,
        "show": show,
        "suppress": suppress,
        "reason": reason,
        "tags": tags,
    })

def rollback(wiki: WikiConnection,
             user: str,
             title: str = None,
             page_id: int = None,
             tags: list[str] = None,
             summary: str = "",
             mark_bot: bool = False,
             watchlist: str = "preferences",
             watchlist_expiry: datetime.datetime = None):
    return wiki.api_with_csrf("rollback", {
        "user": user,
        "title": title,
        "pageid": page_id,
        "tags": tags,
        "summary": summary,
        "markbot": mark_bot,
        "watchlist": watchlist,
        "watchlist_expiry": watchlist_expiry,
    })

def set_notification_timestamp(wiki: WikiConnection,
                               entire_watchlist: bool = False,
                               timestamp: datetime.datetime = None,
                               to_rev_id: int = None,
                               newer_than_rev_id: int = None,
                               should_continue: bool = False,
                               titles: list[str] = None,
                               page_ids: list[int] = None,
                               rev_ids: list[int] = None,
                               generator: str = None,
                               redirects: bool = False,
                               convert_titles: bool = False):
    return wiki.api_with_csrf("setnotificationtimestamp", {
        "entirewatchlist": entire_watchlist,
        "timestamp": timestamp,
        "torevid": to_rev_id,
        "newerthanrevid": newer_than_rev_id,
        "continue": should_continue,
        "titles": titles,
        "pageids": page_ids,
        "revids": rev_ids,
        "generator": generator,
        "redirects": redirects,
        "converttitles": convert_titles,
    })

def tag(wiki: WikiConnection,
        rc_id: list[int] = None,
        rev_id: list[int] = None,
        log_id: list[int] = None,
        add: list[str] = None,
        remove: list[str] = None,
        reason: str = "",
        tags: list[str] = None):
    return wiki.api_with_csrf("tag", {
        "rcid": rc_id,
        "revid": rev_id,
        "logid": log_id,
        "add": add,
        "remove": remove,
        "reason": reason,
        "tags": tags,
    })

def unblock(wiki: WikiConnection,
            user: str,
            uid: int = None,
            reason: str = "",
            tags: list[str] = None):
    return wiki.api_with_csrf("unblock", {
        "user": user,
        "id": uid,
        "reason": reason,
        "tags": tags,
    })

def undelete(wiki: WikiConnection,
             title: str,
             reason: str = "",
             tags: list[str] = None,
             timestamps: list[datetime.datetime] = None,
             file_ids: list[int] = None,
             undelete_talk: bool = False,
             watchlist: str = "preferences",
             watchlist_expiry: datetime.datetime = None):
    return wiki.api_with_csrf("undelete", {
        "title": title,
        "reason": reason,
        "tags": tags,
        "timestamps": timestamps,
        "fileids": file_ids,
        "undeletetalk": undelete_talk,
        "watchlist": watchlist,
        "watchlistexpiry": watchlist_expiry,
    })

def upload(wiki: WikiConnection,
           file_name: str,
           comment: str = "",
           tags: list[str] = None,
           text: str = None,
           watchlist: str = "preferences",
           watchlist_expiry: datetime.datetime = None,
           ignore_warnings: bool = False,
           file: str = None,
           url: str = None,
           file_key: str = None,
           stash: bool = False,
           file_size: int = None,
           offset: int = None,
           do_async: bool = False,
           check_status: bool = False):
    return wiki.api_with_csrf("upload", {
        "filename": file_name,
        "comment": comment,
        "tags": tags,
        "text": text,
        "watchlist": watchlist,
        "watchlistexpiry": watchlist_expiry,
        "ignorewarnings": ignore_warnings,
        "file": file,
        "url": url,
        "filekey": file_key,
        "stash": stash,
        "filesize": file_size,
        "offset": offset,
        "doasync": do_async,
        "checkstatus": check_status
    })    

def user_rights(wiki: WikiConnection,
                user: str,
                add: list[str],
                expiry: list[datetime.datetime] = None,
                remove: list[str] = None,
                reason: str = "",
                tags: list[str] = None):
    return wiki.api_with_csrf("userrights", {
        "user": user,
        "add": add,
        "expiry": expiry,
        "remove": remove,
        "reason": reason,
        "tags": tags,
    })

def watch(wiki: WikiConnection,
          expiry: datetime.datetime,
          unwatch: bool = False,
          should_continue: bool = False,
          titles: list[str] = None,
          page_ids: list[int] = None,
          rev_ids: list[int] = None,
          generator: str = None,
          redirects: bool = False,
          convert_titles: bool = False):
    return wiki.api_with_csrf("watch", {
        "expiry": expiry,
        "unwatch": unwatch,
        "continue": should_continue,
        "titles": titles,
        "pageids": page_ids,
        "revids": rev_ids,
        "generator": generator,
        "redirects": redirects,
        "converttitles": convert_titles,
    })