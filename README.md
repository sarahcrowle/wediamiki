# WediaMiki

WediaMiki is a thin Python wrapper around the MediaWiki Action API.

This code is extremely experimental, and pretty much untested! If you use this, note that things can change at any moment, and some API endpoints are almost certainly misunderstood or wrong.

Not all endpoints have been tested yet! Feel free to raise an issue or PR if you're crazy enough to use this and you run into problems.

MIT License. See [LICENSE.md](LICENSE.md).